import time
import logging
import traceback as tb

from socketserver import TCPServer, BaseRequestHandler
from typing import Tuple

from utils import Packet, PacketType,\
    TaskFarm, NpArrayAttachment, ScoresAttachment
import utils

ServerAddr = Tuple[str, int]
TASKS_PATH = './tasks.json'

# todo: set up logging
# todo: run with argparse
# todo: check that task_id may be str or int everywhere


class GradeHandler(BaseRequestHandler):
    def send_response(self, request_packet: Packet):
        logging.info('start send_response')
        task = TaskFarm.tasks.get(request_packet.task_id)
        scores = task.grade(request_packet.attachment.np_array)

        response_packet = Packet(PacketType.SCORE_RESPONSE,
                                 request_packet.task_id,
                                 ScoresAttachment(scores))

        packed = response_packet.pack()
        self.request.send(packed)
        print('response sent')

    def handle_request(self):
        print('start time', time.time())
        b_request = utils.recv_all(self.request, timeout=0.1)
        req_split = utils.split_packet_bytes(b_request)

        # handle incorrect request
        if not utils.is_score_request(req_split):
            print('handle bad request')
            logging.info('handle bad request')

            error_msg = b'bad request'
            error_response = utils.get_error_response(error_msg)
            self.request.send(error_response)
            return

        # map to concrete handler
        packet = Packet.unpack_by_split(req_split, NpArrayAttachment.unpack)
        self.send_response(packet)

    def handle(self):
        logging.info(f'handle')
        try:
            self.handle_request()
        except Exception:
            error_description = tb.format_exc()
            error_msg = f'error occurs:\n{error_description}\n\n'

            print(error_msg)
            logging.info(error_msg)


def grade_server(addr: ServerAddr):
    TaskFarm(TASKS_PATH)  # Initialization

    with TCPServer(addr, GradeHandler) as server:
        server.serve_forever()


if __name__ == '__main__':
    addr = ('127.0.0.1', 8002)
    grade_server(addr)
