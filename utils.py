import json
import math
import enum
import pickle
import traceback as tb

from functools import partial
from select import select
from typing import List, Tuple, Callable, Dict, Union

import numpy as np
import pandas as pd
from sklearn.metrics import (accuracy_score, f1_score, precision_score,
                             recall_score, roc_auc_score, mean_squared_error)


REQUEST_DELIMITER = '---'
B_REQUEST_DELIMITER = REQUEST_DELIMITER.encode()

NAME_TO_METRIC = {
    # classification
    'accuracy': accuracy_score,

    'binary_f1': f1_score,
    'binary_precision': precision_score,
    'binary_recall': recall_score,
    'binary_roc_auc': roc_auc_score,

    'micro_f1': partial(f1_score, average='micro'),
    'micro_precision': partial(precision_score, average='micro'),
    'micro_recall': partial(recall_score, average='micro'),

    'macro_f1': partial(f1_score, average='macro'),
    'macro_precision': partial(precision_score, average='macro'),
    'macro_recall': partial(recall_score, average='macro'),

    # regression
    'mse': mean_squared_error,
    'rmse': lambda y_true, y_pred: math.sqrt(mean_squared_error(y_true, y_pred))
}

MetricWithName = Tuple[str, Callable]
MapIdToTask = Dict[int, 'Task']
AttachmentUnpacker = Callable[..., 'PacketAttachment']
MetricScore = Tuple[str, float]


# todo: check that task_id may be str or int everywhere


class TaskFarm(object):
    tasks = None

    def __init__(self, path_to_tasks: str):
        if not TaskFarm.tasks:
            TaskFarm.tasks = self.get_tasks(path_to_tasks)

    @staticmethod
    def get_tasks(path_to_tasks: str) -> MapIdToTask:
        with open(path_to_tasks) as f:
            json_tasks = json.load(f)

        tasks = {}
        for json_task in json_tasks:
            task_id = json_task['task_id']
            y_true_path = json_task['y_true_path']
            metrics = get_metrics(json_task['metrics'])

            tasks[task_id] = Task(y_true_path, metrics)

        return tasks


class Task(object):
    def __init__(self, y_true_path: str, metrics: List[MetricWithName],
                 csv_delimiter: str=','):
        self.y_true_path = y_true_path
        self.y_true = np.genfromtxt(y_true_path, delimiter=csv_delimiter)
        self.check_dim(self.y_true)

        self.metrics = metrics

    def check_dim(self, array: np.array):
        if len(array.shape) != 1:
            raise TaskError('y_pred must be 1d array')

    def grade(self, y_pred: np.array) -> List[MetricScore]:
        # Data checking
        self.check_dim(y_pred)
        if y_pred.shape[0] != self.y_true.shape[0]:
            raise TaskError('y_pred must have as many elements as y_true')

        # Evaluation
        return [(metric_name, metric(self.y_true, y_pred))
                for metric_name, metric in self.metrics]


class TaskError(Exception):
    def __init__(self, message: str):
        self.message = message


class PacketType(enum.Enum):
    SCORE_REQUEST = 'score_request'
    SCORE_RESPONSE = 'score_response'


class PacketAttachment(object):
    """Base class for packet attachment"""

    def pack(self) -> bytes:
        raise NotImplemented

    @staticmethod
    def unpack(bytes_: bytes) -> 'PacketAttachment':
        raise NotImplemented


class NpArrayAttachment(PacketAttachment):
    def __init__(self, np_array: np.array):
        if type(np_array) is not np.ndarray:
            raise TypeError('np_array must be np.array instance')
        self.np_array = np_array

    def pack(self):
        return self.np_array.dumps()

    @staticmethod
    def unpack(np_array_bytes: bytes) -> 'NpArrayAttachment':
        return NpArrayAttachment(np.loads(np_array_bytes))


class ScoresAttachment(PacketAttachment):
    def __init__(self, scores: List[MetricScore]):
        self.scores = scores

    def pack(self):
        return pickle.dumps(self.scores)

    @staticmethod
    def unpack(scores_bytes: bytes) -> 'ScoresAttachment':
        return ScoresAttachment(pickle.loads(scores_bytes))


class Packet(object):
    def __init__(self, packet_type: PacketType, task_id: Union[int, str],
                 attachment: PacketAttachment):
        self.packet_type = packet_type
        self.task_id = task_id
        self.attachment = attachment

    def pack(self):
        return B_REQUEST_DELIMITER.join([self.packet_type.value.encode(),
                                         str(self.task_id).encode(),
                                         self.attachment.pack()])

    @staticmethod
    def unpack_by_split(split: List[bytes],
                        unpacker: AttachmentUnpacker) -> 'Packet':
        packet_type = PacketType(split[0].decode())
        attachment = unpacker(split[2])

        if split[1].isdigit():
            task_id = int(split[1])
        else:
            task_id = split[1].decode()

        return Packet(packet_type, task_id, attachment)

    @staticmethod
    def unpack(data: bytes, unpacker: AttachmentUnpacker) -> 'Packet':
        split = data.split(B_REQUEST_DELIMITER)
        return Packet.unpack_by_split(split, unpacker)


def get_error_response(msg: bytes) -> bytes:
    return B_REQUEST_DELIMITER.join([b'error', msg])


def recv_all(obj_with_recv, recv_size: int=8196, timeout: float=0.1) -> bytes:
    # check recv
    reads, _, _ = select([obj_with_recv], [], [], timeout)
    if not reads:
        return b''

    # get data with recv
    data = batch = obj_with_recv.recv(recv_size)
    while len(batch) == recv_size:
        batch = obj_with_recv.recv(recv_size)
        data += batch
    return data


def split_packet_bytes(bytes_: bytes) -> List[bytes]:
    return bytes_.split(B_REQUEST_DELIMITER)


def get_metrics(metric_names: List[str]) -> List[MetricWithName]:
    return [(metric_name, NAME_TO_METRIC[metric_name])
            for metric_name in metric_names]


def cast_to_ndarray(y_pred: Union[np.ndarray, pd.Series]) -> np.ndarray:
    if not is_ndarray_or_series(y_pred):
        raise TypeError('y_pred must be np.ndarray or pd.Series instance')
    return np.array(y_pred)


def is_score_packet(packet_type: bytes, split: List[bytes]) -> bool:
    return len(split) == 3 and split[0] == packet_type


def is_correct_attachment(attach_bytes: bytes,
                          unpacker: AttachmentUnpacker) -> bool:
    try:
        unpacker(attach_bytes)
    except:
        print(tb.format_exc())
        return False
    return True


def is_score_request(split: List[bytes]) -> bool:
    packet_type = PacketType.SCORE_REQUEST.value.encode()
    return (is_score_packet(packet_type, split) and
            is_correct_attachment(split[2], NpArrayAttachment.unpack))


def is_score_response(split: List[bytes]) -> bool:
    packet_type = PacketType.SCORE_RESPONSE.value.encode()
    return (is_score_packet(packet_type, split) and
            is_correct_attachment(split[2], ScoresAttachment.unpack))


def is_error_response(split: List[bytes]) -> bool:
    return len(split) >= 1 and split[0] == b'error'


def is_ndarray_or_series(obj: object) -> bool:
    return (type(obj) is np.ndarray) or (type(obj) is pd.Series)
