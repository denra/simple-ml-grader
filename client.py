import socket
from typing import Union, List

import numpy as np
import pandas as pd

from utils import Packet, NpArrayAttachment, ScoresAttachment,\
    PacketType, MetricScore
import utils


class Grader(object):
    def __init__(self, server_address=('127.0.0.1', 8001)):
        self.server_address = server_address

    def grade_first_task(self, y_pred: Union[np.array, pd.Series]) -> float:
        y_pred_array = utils.cast_to_ndarray(y_pred)
        scores = self.grade_task(1, y_pred_array)
        return dict(scores).get('accuracy')

    def grade_task(self, task_id: Union[str, int], y_pred: np.array) -> List[MetricScore]:
        attachment = NpArrayAttachment(y_pred)
        packet = Packet(PacketType.SCORE_REQUEST, task_id, attachment)

        with socket.create_connection(self.server_address) as client:
            client.send(packet.pack())
            print('request is sent')

            b_response = utils.recv_all(client, timeout=2)
            print('response is received')
        resp_split = utils.split_packet_bytes(b_response)

        if not utils.is_score_response(resp_split):
            if utils.is_error_response(resp_split):
                print(*list(map(lambda x: x.decode(), resp_split)), sep=' | ')
            else:
                print('incorrect response')
            return []

        resp_packet = Packet.unpack_by_split(resp_split,
                                             ScoresAttachment.unpack)
        return resp_packet.attachment.scores


def main():
    grader = Grader(server_address=('127.0.0.1', 8001))

    df = pd.DataFrame({'target': [1, 1, 2]})
    score = grader.grade_first_task(df.target)
    
    print('accuracy_score:', score)


if __name__ == '__main__':
    main()
