# "Client-server" specification
## Requests to server
1. `score_request---<task_id>---<pickle_np_array>`

## Replies from server
1. `score_response---<task_id>---<score>`
2. `error---<error_msg>`

